FROM node:lts-slim AS build

RUN apt update; apt install -y git
# RUN git clone https://framagit.org/framasoft/framadate/funky-framadate-front/ /var/www/html/funky-framadate-front

WORKDIR /var/www/html
RUN chown -R www-data /var/www
USER www-data
# WORKDIR /var/www/html/funky-framadate-front
COPY yarn.lock package.json angular.json tsconfig.json tsconfig.app.json ./
RUN yarn install --pure-lockfile

ENV NODE_OPTIONS=--openssl-legacy-provider

COPY --chown=www-data:www-data ./mocks ./mocks
COPY --chown=www-data:www-data ./src ./src
RUN yarn run build:prod

# Unused (in case you use the intermediate image for dev purposes)
FROM build as dev
EXPOSE 4200
CMD [ "bash", "docker/dev/entrypoint.sh"]

# Only serve static files
FROM nginx
COPY --from=0 --chown=www-data:www-data /var/www/html/dist/framadate/* /usr/share/nginx/html