import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PollResultsDinumComponent } from './poll-results-dinum.component';

describe('PollResultsDinumComponent', () => {
	let component: PollResultsDinumComponent;
	let fixture: ComponentFixture<PollResultsDinumComponent>;

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			declarations: [PollResultsDinumComponent],
		}).compileComponents();
	}));

	beforeEach(() => {
		fixture = TestBed.createComponent(PollResultsDinumComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('should create', () => {
		expect(component).toBeTruthy();
	});
});
